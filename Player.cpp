#include <iostream>
#include "Player.h"

using namespace std;

void Player::EnterData()
{
	cin >> name >> score;
}

void Player::ShowData()
{
	cout << name << "\t" << score << endl;
}
